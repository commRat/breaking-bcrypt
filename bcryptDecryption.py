import bcrypt
import sqlite3
import time

# Connect DB
connection = sqlite3.connect('passwords.db') # For master login

# Define cursor
c = connection.cursor() # For sword 

# Create tables 
c.execute('CREATE TABLE IF NOT EXISTS passwords(password VARCHAR NOT NULL)')

# List of common passwords
guess = ['password', 'password123', 'Password1', '1234657', 'wgrver', 'qwerty',
         'abc123', 'iloveyou', '000000', '000100', 'dragon', 'princess', 'monkey',
         'zaq12wsx', 'letmein', '1qaz2wsx', 'superman', '123321', '1q2w3e4r',
         'wefegrerggetregr']

pw = input('Insert password: ')
password = pw.encode('utf-8')

hashed = bcrypt.hashpw(password, bcrypt.gensalt()) # Generate salt
decoded = hashed.decode() # Decode salt hashed password before putting it to DB

# Put password to db (salt hashed format)
put = '''INSERT INTO passwords(password)VALUES (?)'''
c.execute(put,[(decoded)])


print('Looking for your password')

# Pull inserted password from DB in salt hashed format
c.execute('SELECT password FROM passwords')
pulled = c.fetchone()
encoded_password = pulled[0].encode('utf-8') # Encode password

# found variable will switch to True if program break the password
found = False

# Check each element in list if that is correct password - if It is, print password
i = 0
for i in range(len(guess)):
        enc_from_ls = guess[i].encode('utf-8')
        if bcrypt.checkpw(enc_from_ls, encoded_password):
            real_password = enc_from_ls.decode('utf-8')
            print(f'The password before salt hash: {real_password}')
            found = True
            break
        i += 1
        
if found == False:
    print('Program did not manage to break the password')
else:
    print('\nSave the password above. It is password before it was salt hashed.')

# Delete stored password    
delete = '''DELETE password FROM passwords'''

time.sleep(3)