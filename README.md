## Breaking bcrypt

This script using the 'brute force' to crack password - which is stored in database with salt hashed format. This is only the concept how that works - to make it really usefull we need a lot of data (common passwords) and high computing power, to crack it fast.

## How it works:

1) Define a list of few common passwords
2) User input password 
3) That password is stored in database, after It's hashed (so in DB we can see only salt hashed format of that password)
4) Program takes each element from list (1), stored password from DB and compares it, using bcrypt
5) If comparsion returns True, It means that user inputed password was in the list and show it - so we get password input (from hash), before it was hashed
