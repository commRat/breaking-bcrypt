import hashlib


text_to_hash = hashlib.sha256(b'moon')
hash_text = text_to_hash.hexdigest()

# 64 symbols
print(f'Number of symbols: {len(hash_text)}')

# Generated for 'moon' from https://emn178.github.io/online-tools/sha256.html
moon = '9e78b43ea00edcac8299e0cc8df7f6f913078171335f733a21d5d911b6999132'

# Check if sha256 has always the same output (hash) for same string
if moon == hash_text:
    print('moon = 9e78b43ea00edcac8299e0cc8df7f6f913078171335f733a21d5d911b6999132')
else:
    print('Not the same output.')

